FROM containers.ligo.org/lscsoft/lalsuite:nightly

RUN echo "Building lalinference"
MAINTAINER John Veitch <john.veitch@ligo.org>

# Dependencies: MPI requires ssh (or maybe rsh)
RUN apt-get update && \
    apt-get --assume-yes upgrade && \
    apt-get --assume-yes install ssh openmpi-bin git && \
    cd /tmp && git clone https://git.ligo.org/lscsoft/pylal.git && cd pylal && \
    python setup.py install && cd - && rm -r /tmp/pylal

WORKDIR /


# Create cvmfs dir for importing frames (if permitted)
#RUN mkdir -p /cvmfs/oasis.opensciencegrid.org/ligo/frames
RUN mkdir -p /cvmfs
# Create directory for condor to execute in
#RUN mkdir -p /local/condor/execute/
# Point to lalsuite-extra for ROM data
#RUN mkdir -p /cvmfs/oasis.opensciencegrid.org/ligo/sw/pycbc/lalsuite-extra/current/share/lalsimulation
#ENV LAL_DATA_PATH /cvmfs/oasis.opensciencegrid.org/ligo/sw/pycbc/lalsuite-extra/current/share/lalsimulation

